var win = typeof window !== 'undefined' ? window : {};

export var caf = win.cancelAnimationFrame
  || win.mozCancelAnimationFrame
  || function(id){ clearTimeout(id); };
